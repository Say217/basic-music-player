package id.ac.ui.cs.mobileprogramming.sayid_abyan_rizal_shiddiq.basic_music_player

import android.content.Context
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.widget.TextView
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import android.Manifest.permission
import android.app.Notification
import android.app.NotificationChannel
import android.content.Intent
import android.media.MediaPlayer
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Build
import android.widget.Toast
import android.app.NotificationManager as NotificationManager1
import id.ac.ui.cs.mobileprogramming.sayid_abyan_rizal_shiddiq.basic_music_player.webActivity as webActivity


class MainActivity : AppCompatActivity() {
    val CHANNEL_ID: String = "BMPNotificationChannel"
    private var STORAGE_PERMISSION_CODE = 1
    var playPauseButton: FloatingActionButton? = null
    var infoButton: FloatingActionButton? = null
    var randomButton: FloatingActionButton? = null
    var songName: TextView? = null
    var songArtist: TextView? = null
    var recyclerView: RecyclerView? = null
    var getSongList: ArrayList<Song>? = null
    var _songAdapter: SongAdapter? = null
    var mediaplayer: MediaPlayer? = null
    var currentSong: Song? = null
    var currentPosition: Int = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        playPauseButton = findViewById(R.id.play_pause)
        infoButton = findViewById(R.id.info)
        randomButton = findViewById(R.id.random)
        songName = findViewById(R.id.song_name)
        songArtist = findViewById(R.id.song_artist)
        recyclerView = findViewById(R.id.song_list)
        mediaplayer = MediaPlayer()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create the NotificationChannel
            val name = "BMP"
            val descriptionText = "Notification channel for BMP"
            val importance = NotificationManager1.IMPORTANCE_LOW
            val mChannel = NotificationChannel(CHANNEL_ID, name, importance)
            mChannel.description = descriptionText
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager1
            notificationManager.createNotificationChannel(mChannel)
        }

        if (checkSelfPermission(permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermission()
        }
        else{
            getSongList = getSongFromPhone()
            _songAdapter = SongAdapter(getSongList as ArrayList<Song>, this)
            val mLayoutManager = LinearLayoutManager(this)
            recyclerView?.layoutManager = mLayoutManager
            recyclerView?.itemAnimator = DefaultItemAnimator()
            recyclerView?.adapter = _songAdapter
        }

        clickHandler()
    }

    private fun requestPermission() {
       requestPermissions( arrayOf(permission.READ_EXTERNAL_STORAGE), STORAGE_PERMISSION_CODE)
    }
    fun getSongFromPhone(): ArrayList<Song>{
        var arrayList = ArrayList<Song>()
        var contentResolver = this.contentResolver
        var songUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
        var songCursor = contentResolver?.query(songUri, null, null, null, null)
        if(songCursor != null && songCursor.moveToFirst()){
            val songId = songCursor.getColumnIndex(MediaStore.Audio.Media._ID)
            val songTitle = songCursor.getColumnIndex(MediaStore.Audio.Media.TITLE)
            val songArtist = songCursor.getColumnIndex(MediaStore.Audio.Media.ARTIST)
            val songData = songCursor.getColumnIndex(MediaStore.Audio.Media.DATA)
            val dateIndex = songCursor.getColumnIndex(MediaStore.Audio.Media.DATE_ADDED)
            while(songCursor.moveToNext()){
                var currentId = songCursor.getLong(songId)
                var currentTitle = songCursor.getString(songTitle)
                var currentArtist = songCursor.getString(songArtist)
                var currentData = songCursor.getString(songData)
                var currentDate = songCursor.getLong(dateIndex)
                arrayList.add(Song(currentId, currentTitle, currentArtist, currentData, currentDate))
            }
        }
        return arrayList
    }

    fun startSong(){
        mediaplayer?.start()
        playPauseButton?.setImageResource(R.drawable.ic_pause_24dp)
        createNotification()
    }

    fun createNotification(){
        var notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager1
        var notifyId = 1
        var notification = Notification.Builder(this)
        .setContentTitle(currentSong?.songTitle)
        .setContentText(currentSong?.artist)
        .setSmallIcon(R.drawable.ic_play_arrow_24dp)
        .setChannelId(CHANNEL_ID)
        .build()
        notificationManager.notify(notifyId, notification)
    }

    fun checkInternetConnection(): Boolean {
        var cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        var status = cm.activeNetworkInfo != null && cm.activeNetworkInfo.isAvailable && cm.activeNetworkInfo.isConnected
        if (!status)
            Toast.makeText(this, "Internet Connection Detected", Toast.LENGTH_SHORT).show()
        return status
    }

    fun googleSong(){
        intent = Intent(this, webActivity::class.java)
        intent.putExtra("toBeGoogled", currentSong?.artist)
        startActivity(intent)
    }

    fun clickHandler(){
        playPauseButton?.setOnClickListener {
            if(mediaplayer?.isPlaying as Boolean){
                mediaplayer?.pause()
                playPauseButton?.setImageResource(R.drawable.ic_play_arrow_24dp)
            }
            else{
                startSong()
            }
        }

        randomButton?.setOnClickListener({
            mediaplayer?.reset()
            currentPosition = nativeRandom(getSongList!!.size)
            currentSong = getSongList?.get(currentPosition)
            mediaplayer?.setDataSource(this, Uri.parse(currentSong?.songData))
            mediaplayer?.prepare()
            startSong()
        })
        infoButton?.setOnClickListener({
            if(!checkInternetConnection()){
                Toast.makeText(this, "No Internet Connection Detected", Toast.LENGTH_SHORT).show()
            }
            else{
                googleSong()
            }
        })

        mediaplayer?.setOnCompletionListener {
            mediaplayer?.reset()
            currentPosition += 1
            if(currentPosition == getSongList?.size){
                currentPosition = 0
            }
            currentSong = getSongList?.get(currentPosition)
            mediaplayer?.setDataSource(this, Uri.parse(currentSong?.songData))
            mediaplayer?.prepare()
            startSong()
        }
    }

    override fun onDestroy() {
        mediaplayer?.stop()
        super.onDestroy()
    }

    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    external fun nativeRandom(range: Int): Int

    companion object {

        // Used to load the 'native-lib' library on application startup.
        init {
            System.loadLibrary("native-lib")
        }
    }
}
