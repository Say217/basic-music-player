#include <jni.h>
#include <string>

extern "C" JNIEXPORT jint JNICALL
Java_id_ac_ui_cs_mobileprogramming_sayid_1abyan_1rizal_1shiddiq_basic_1music_1player_MainActivity_nativeRandom(
        JNIEnv *env,
        jobject /* this */,
        jint range) {
    return (jint) (rand()%range);
}

