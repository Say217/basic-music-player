package id.ac.ui.cs.mobileprogramming.sayid_abyan_rizal_shiddiq.basic_music_player


import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView

class SongAdapter(_songDetails: ArrayList<Song>, _context: Context): RecyclerView.Adapter<SongAdapter.MyViewHolder>(){

    var songDetails: ArrayList<Song>? = null
    var mContext: Context? = null

    init{
        this.songDetails = _songDetails
        this.mContext = _context

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.song_card, parent, false)

        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val songObject= songDetails?.get(position)
        holder.trackTitle?.text = songObject?.songTitle
        holder.trackArtist?.text = songObject?.artist
        holder.trackCard?.setOnClickListener {
            val activity = mContext as MainActivity
            activity.currentSong = songObject
            activity.currentPosition = position
            if(activity.mediaplayer?.isPlaying as Boolean){
                activity.mediaplayer?.reset()
            }
            activity.mediaplayer?.setDataSource(activity, Uri.parse(songObject?.songData))
            activity.mediaplayer?.prepare()
            activity.startSong()
        }
    }

    override fun getItemCount(): Int {
        if(songDetails == null){
            return 0
        }else{
            return (songDetails as ArrayList<Song>).size
        }
    }

    class MyViewHolder(view: View): RecyclerView.ViewHolder(view){
        var trackTitle:  TextView? = null
        var trackArtist: TextView? = null
        var trackCard: CardView? = null

        init{
            trackTitle = view.findViewById(R.id.song_name)
            trackArtist = view.findViewById(R.id.song_artist)
            trackCard = view.findViewById(R.id.song_card)
        }
    }
}